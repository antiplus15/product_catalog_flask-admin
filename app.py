from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask import Flask, render_template_string, url_for, redirect, render_template, request, abort
from flask_admin.contrib.sqla.filters import ChoiceTypeEqualFilter
from flask_admin.model.filters import BaseFilter
from flask_security import Security, current_user, auth_required, hash_password, \
     SQLAlchemySessionUserDatastore
from wtforms import StringField

from database import db_session, init_db
from models import User, Role, Product, Address
from flask_admin import helpers as admin_helpers
from flask_security.forms import RegisterForm, Required

# Create app
app = Flask(__name__)
app.config.from_pyfile('config.py')


# Setup Flask-Security
class ExtendedRegisterForm(RegisterForm):
    username = StringField('User name', [Required()])


user_datastore = SQLAlchemySessionUserDatastore(db_session, User, Role)
security = Security(app,
                    user_datastore,
                    register_form=ExtendedRegisterForm
                    )


# Create a user to test
@app.before_first_request
def create_user():
    init_db()

    if not user_datastore.find_role(role='user'):
        user_datastore.create_role(name='user')
        db_session.commit()

    if not user_datastore.find_role(role='superuser'):
        user_datastore.create_role(name='superuser')
        db_session.commit()

    user_role = user_datastore.find_role(role='user')
    super_user_role = user_datastore.find_role(role='superuser')

    if not user_datastore.find_user(email="superuser@test.com"):
        user_datastore.create_user(
            username='superuser',
            email="superuser@test.com",
            password=hash_password("superpassword"),
            roles=[user_role, super_user_role]
        )
        db_session.commit()

    # if not user_datastore.find_user(email="user@test.com"):
    #     user_datastore.create_user(
    #         username='user',
    #         email="user@test.com",
    #         password=hash_password("password"),
    #         roles=[user_role]
    #     )
    #     db_session.commit()
# Views


# Create customized model view class
class UserModelView(ModelView):
    def is_accessible(self):
        return (current_user.is_active and
                current_user.is_authenticated and
                current_user.has_role('user')
                )

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))


class SuperUserModelView(UserModelView):
    def is_accessible(self):
        return (current_user.is_active and
                current_user.is_authenticated and
                current_user.has_role('superuser')
                )


class ProductModelView(UserModelView):
    column_filters = ['address']
    # column_hide_backrefs = False
    column_list = ('product_name', 'colour', 'weight', 'price', 'address')
    # column_display_all_relations = True

admin = Admin(app,
              name='product catalog',
              base_template='my_master.html',
              template_mode='bootstrap4'
              )
admin.add_view(SuperUserModelView(Role, db_session))
admin.add_view(SuperUserModelView(User, db_session))
admin.add_view(ProductModelView(Product, db_session))
admin.add_view(UserModelView(Address, db_session))


@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )


# Flask views
@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()
