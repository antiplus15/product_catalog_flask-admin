from datetime import datetime
from database import Base
from flask_security import UserMixin, RoleMixin
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Boolean, DateTime, Column, Integer, \
                       String, ForeignKey, NUMERIC, Table


class RolesUsers(Base):
    __tablename__ = 'roles_users'
    id = Column(Integer(), primary_key=True)
    user_id = Column('user_id', Integer(), ForeignKey('user.id'))
    role_id = Column('role_id', Integer(), ForeignKey('role.id'))


class Role(Base, RoleMixin):
    __tablename__ = 'role'
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    def __repr__(self):
        return self.name


class User(Base, UserMixin):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    username = Column(String(255), unique=True, nullable=False)
    password = Column(String(255), nullable=False)
    last_login_at = Column(DateTime())
    current_login_at = Column(DateTime(), default=datetime.utcnow)
    last_login_ip = Column(String(100))
    current_login_ip = Column(String(100))
    login_count = Column(Integer)
    active = Column(Boolean())
    fs_uniquifier = Column(String(255), unique=True, nullable=False)
    confirmed_at = Column(DateTime(), default=datetime.utcnow)
    roles = relationship('Role', secondary='roles_users',
                         backref=backref('users', lazy='dynamic'))


class ProductAddress(Base):
    __tablename__ = 'product_address'
    id = Column(Integer(), primary_key=True)
    product_id = Column('product_id', Integer(), ForeignKey('product.id'))
    address_id = Column('address_id', Integer(), ForeignKey('address.id'))


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    product_name = Column(String(80), unique=True, nullable=False)
    colour = Column(String(80), nullable=False)
    weight = Column(NUMERIC(10, 3), nullable=False)
    price = Column(NUMERIC(10, 2), nullable=False)
    address_id = Column(Integer,
                        ForeignKey('address.id'),
                        nullable=True
                        )

    address = relationship('Address', secondary='product_address',
                           backref=backref('product', lazy='dynamic')
                           )

    def __repr__(self):
        return f'{self.product_name}, {self.address}'


class Address(Base):
    __tablename__ = 'address'
    id = Column(Integer, primary_key=True)
    full_name = Column(String(255), nullable=False)
    street_address = Column(String(255), nullable=False)
    city = Column(String(255), nullable=False)
    postal_code = Column(String(255), nullable=False)
    country = Column(String(255), nullable=False)

    def __repr__(self):
        return f'{self.full_name}, {self.city}, {self.country}'

